
#include <stdint.h>
#include <rt_mmap.h>
#include <stddef.h>
#include "serial.h"
#include "tinyprintf.h"
#include "types.h"
#include "tests.h"

uint32_t seed = 123456789;

uint32_t a = 1103515245;
uint32_t c = 12345;
uint32_t m = 0x80000000;
size_t physaddrbase;

extern uint32_t begin; // beginning of free memory

uint32_t rand()
{
    seed = (a * seed + c) % m;
    return seed;
}

int serial_init (void);
void serial_putc (const char c);

void _putc ( void* p, char c)
{
    serial_putc(c);
}

int putchar(int c) {
    serial_putc(c);
    return 0;
}

struct test tests[] = {
    { "Random Value", test_random_value },
    { "Compare XOR", test_xor_comparison },
    { "Compare SUB", test_sub_comparison },
    { "Compare MUL", test_mul_comparison },
    { "Compare DIV",test_div_comparison },
    { "Compare OR", test_or_comparison },
    { "Compare AND", test_and_comparison },
    { "Sequential Increment", test_seqinc_comparison },
    { "Solid Bits", test_solidbits_comparison },
//    { "Block Sequential", test_blockseq_comparison },
//    { "Checkerboard", test_checkerboard_comparison },
//    { "Bit Spread", test_bitspread_comparison },
//    { "Bit Flip", test_bitflip_comparison },
//    { "Walking Ones", test_walkbits1_comparison },
//    { "Walking Zeroes", test_walkbits0_comparison },
#ifdef TEST_NARROW_WRITES    
    { "8-bit Writes", test_8bit_wide_random },
    { "16-bit Writes", test_16bit_wide_random },
#endif
    { NULL, NULL }
};


void setup() {
    // set uart-lite multiplex to peripherial instead of gpio
    *(uint32_t*)(RALINK_SYSCTL_BASE + 0x60) = *(uint32_t*)(RALINK_SYSCTL_BASE + 0x60) & ~(1 << 5);

    serial_init();
    init_printf((void*)0, _putc);
}

void main(void) {
    setup();
    printf("\nMemtest begin\n");
    volatile long unsigned int* aligned = (void*)&begin;
    uint32_t exit_code = 0;
    physaddrbase = (size_t)aligned;
    size_t bufsize = 64*1024*1024 - ((uint32_t)&begin - 0x80000000);
    size_t halflen = ((bufsize / 2) & 0xfffffffc) + sizeof(ul);
    size_t count = halflen / sizeof(ul);
    volatile long unsigned int * bufa = (ulv *) aligned;
    volatile long unsigned int* bufb = (ulv *) ((size_t) aligned + halflen);

    printf("begin: 0x%X, bufsize: %d, bufa: 0x%X, bufb: 0x%X, halflen: 0x%X\n", &begin, bufsize, bufa, bufb, halflen );

    printf("  %-20s: ", "Stuck address");
    if (!test_stuck_address(aligned, bufsize / sizeof(ul))) {
	printf("\t\tok\n");
    } else {
        exit_code = 2;
        goto out;
    }
    for (int i=0;;i++) {
        if (!tests[i].name) break;
        /* If using a custom testmask, only run this test if the
           bit corresponding to this test was set by the user.
         */
        printf("  %-20s: ", tests[i].name);
        if (!tests[i].fp(bufa, bufb, count)) {
            printf("\t\tok\n");
        } else {
            exit_code = 1;
            goto out;
        }
    }
out:
    if( exit_code != 0 ) {
	printf("\nFAIL\n");
    } else {
	printf("\nOKAY\n");
    }

    while(1);
}
