CC=$(CROSS_COMPILE)gcc
CXX=$(CROSS_COMPILE)cpp
AS=$(CROSS_COMPILE)as
LD=$(CROSS_COMPILE)ld

CFLAGS=-DMT7620_ASIC_BOARD -ggdb3 -I./ -I./include -EL -mips32r2 -O1
ASFLAGS=-DMT7620_ASIC_BOARD -ggdb3 -I./ -I./include -EL -mips32r2
LDFLAGS=

OBJS:= start.o serial.o tinyprintf.o main.o tests.o

%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $< 

memtest: $(OBJS)
	$(LD) $(LDFLAGS) -T memtest.lds $(OBJS) libgcc.a  -o memtest

all: memtest

.PHONY: clean

clean:
	rm -f *.o memtest
